# RECUNOAȘTEREA ALIMENTELOR ȘI ESTIMAREA CALORIILOR FOLOSIND DEEP LEARNING

**Despre proiect**

Acest proiect are scopul de a recunoaște alimentele din imagini și de a estima caloriile acestora folosind tehnici de deep learning. Am utilizat MobileNet, o arhitectură optimizată pentru eficiență pe dispozitive mobile și resurse limitate. Proiectul include colectarea datelor, antrenarea modelului și dezvoltarea unei interfețe web pentru utilizatori.

**Configurarea Mediului de Dezvoltare**

Pentru a începe, asigurați-vă că aveți Python instalat. Puteți descărca Python de la python.org. Este recomandat să utilizați Python 3.7 sau o versiune mai nouă.

După instalarea Python, va trebui să instalați următoarele module:

pip install numpy==1.20.3
pip install Keras==2.9.0
pip install tensorflow==2.9.0
pip install Flask==2.1.2
pip install pandas==1.4.3
pip install matplotlib==3.4.3
pip install Werkzeug==2.2.2


**Descarcă și Pregătește Setul de Date**


1.Descarcă setul de date Food 101 de pe Kaggle: https://www.kaggle.com/datasets/dansbecker/food-101

2.Rulează scriptul _get_nutrition_data.py_ pentru a genera baza de date nutrition101.csv cu informații nutriționale.



**Antrenează Modelul**

1.Modifica in scriptul _train.py_ path-urile pentru variabilele train_dir si test_dir

2.Rulează scriptul _train.py_ pentru a configura și antrena modelul MobileNet.

3.După antrenare, modelul va fi salvat în fișierul food.h5.

**Rularea Aplicației Web**

1.Rulează scriptul app.py pentru a porni serverul web Flask.

2.Deschide linkul local host generat (de obicei http://127.0.0.1:5000/) pentru a accesa aplicația web și a începe să încarci imagini.




